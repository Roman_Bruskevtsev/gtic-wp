<?php
/**
 * @package WordPress
 * @subpackage GTIC
 * @since 1.0
 * @version 1.0
 */
$footer = get_field('footer', 'option');
$background = $footer['background_image'] ? ' style="background-image: url('.$footer['background_image'].')"' : '';
$anchor = $footer['anchor'] ? ' id="'.$footer['anchor'].'"' : '';
?>  
    </main>
    <footer class="gti-footer"<?php echo $background; ?>>
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="anchor"<?php echo $anchor; ?>></div>
                    <div class="gti-section__title noseparator" data-aos="fade-up" data-aos-duration="500">
                        <?php if( $footer['small_title'] ) { ?>
                            <h4 class="font__red"><?php echo $footer['small_title']; ?></h4>
                        <?php }
                        if( $footer['title'] ) { ?>
                            <h2><?php echo $footer['title']; ?></h2>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-lg-1"></div>
                <div class="col-md-5 col-lg-3">
                    <?php if( $footer['copyright'] ) { ?>
                        <div class="copyright" data-aos="fade-up" data-aos-duration="500"><?php echo $footer['copyright']; ?></div>
                    <?php } 
                    if( $footer['phone'] ) { ?>
                        <a class="phone" href="tel:<?php echo $footer['phone']; ?>" data-aos="fade-up" data-aos-duration="500"><?php echo $footer['phone']; ?></a>
                    <?php } 
                    if( $footer['email'] ) { ?>
                        <a class="email" href="mailto:<?php echo $footer['email']; ?>" data-aos="fade-up" data-aos-duration="500"><?php echo $footer['email']; ?></a>
                    <?php } ?>
                </div>
                <?php if( $footer['addresses'] ) { ?>
                <div class="col-md-1"></div>
                <div class="col-md-5 col-lg-4">
                    <?php foreach ( $footer['addresses'] as $address ) { ?>
                     <div class="address__block" data-aos="fade-up" data-aos-duration="500">
                        <?php if( $address['title'] ) { ?>
                            <h4><?php echo $address['title']; ?></h4>
                        <?php } 
                        echo $address['text']; ?>
                     </div>
                    <?php } ?>
                </div>
                <?php } ?>
            </div>
        </div>
    </footer>
    <?php wp_footer(); ?>
</body>
</html>