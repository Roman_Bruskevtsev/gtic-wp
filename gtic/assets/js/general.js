'use strict';

class GeneralClass{
    constructor(){
        this.init();
    }

    init(){
        this.documentReady();
    }

    documentReady(){
        document.addEventListener('DOMContentLoaded', function(){
            AOS.init();
            general.sparksJs();
            general.headerInit();
            general.scrollDownInit();
            general.aboutSliderInit();
            general.benefitsSliderInit();
            general.productionSliderInit();
        });
    }

    sparksJs(){
        particlesJS('hero', {
          "particles":{
            "number":{
              "value":500,
              "density":{
                "enable":true,
                "value_area":3000
              }
            },
            "color":{
              "value":"#fd7907"
            },
            "shape":{
              "type":"circle",
              "stroke":{
                "width":0,
                "color":"#000000"
              },
              "polygon":{
                "nb_sides":3
              },
              "image":{
                "src":"img/github.svg",
                "width":100,
                "height":100
              }
            },
            "opacity":{
              "value":0.8,
              "random":true,
              "anim":{
                "enable":false,
                "speed":1,
                "opacity_min":0.1,
                "sync":false
              }
            },
            "size":{
              "value":4,
              "random":true,
              "anim":{
                "enable":true,
                "speed":5,
                "size_min":0.01,
                "sync":false
              }
            },
            "line_linked":{
              "enable":false,
              "distance":500,
              "color":"#ffffff",
              "opacity":0.4,
              "width":1
            },
            "move":{
              "enable":true,
              "speed":7.8,
              "direction":"top",
              "random":true,
              "straight":false,
              "out_mode":"out",
              "bounce":false,
              "attract":{
                "enable":false,
                "rotateX":600,
                "rotateY":1200
              }
            }
          },"interactivity":{
            "detect_on":"canvas",
            "events":{
              "onhover":{
                "enable":false,
                "mode":"bubble"
              },
              "onclick":{
                "enable":false,
                "mode":"repulse"
              },
              "resize":true
            },
            "modes":{
              "grab":{
                "distance":400,
                "line_linked":{
                  "opacity":0.5
                }
              },
              "bubble":{
                "distance":400,
                "size":4,
                "duration":0.3,
                "opacity":1,
                "speed":3
              },
              "repulse":{
                "distance":200,
                "duration":0.4
              },
              "push":{
                "particles_nb":4
              },
              "remove":{
                "particles_nb":2
              }
            }
          },
          "retina_detect":true
        });
    }

    headerInit(){
        let header = document.querySelector('.gti-header'),
            mobileBtn = document.querySelector('.gti-mobile__btn'),
            mobileMenu = document.querySelector('.gti-mobile__nav'),
            menuPoints = mobileMenu.querySelectorAll('a'),
            smoothMenuPoints = document.querySelectorAll('.gti-main__nav li a[href^="#"]'),
            smoothMobileMenuPoints = document.querySelectorAll('.gti-mobile__nav li a[href^="#"]'),
            topScroll = window.scrollY;

        if( topScroll > 100 ){
            header.classList.add('scroll');
            mobileMenu.classList.add('scroll');
        } else {
            header.classList.remove('scroll');
            mobileMenu.classList.remove('scroll');
        }

        window.addEventListener('scroll', function() {
            topScroll = window.scrollY;

            if( topScroll > 100 ){
                header.classList.add('scroll');
                mobileMenu.classList.add('scroll');
            } else {
                header.classList.remove('scroll');
                mobileMenu.classList.remove('scroll');
            }
        });

        mobileBtn.addEventListener('click', () => {
            header.classList.toggle('show__menu');
            mobileBtn.classList.toggle('show__menu');
            mobileMenu.classList.toggle('show__menu');
        });

        menuPoints.forEach( (item) => {
            item.addEventListener('click', () => {
                menuPoints.forEach( (point) => {
                    point.classList.remove('active');
                });
                item.classList.add('active');
                header.classList.remove('show__menu');
                mobileBtn.classList.remove('show__menu');
                mobileMenu.classList.remove('show__menu');
            });
        });

        if( smoothMenuPoints ){
            let headerHeight, pointHref, pointTopHeight, siteUrl, newUrl;
            smoothMenuPoints.forEach( (point) => {
                point.addEventListener('click', (e) => {
                    e.preventDefault();
                    headerHeight = header.offsetHeight;
                    pointHref = point.getAttribute('href');
                    pointTopHeight = document.querySelector(pointHref).getBoundingClientRect().top + window.scrollY - headerHeight - 30;
                    siteUrl = document.URL.split('#')[0];
                    newUrl = siteUrl + pointHref;

                    window.history.pushState({}, '', newUrl);

                    window.scrollTo({
                        top: pointTopHeight,
                        behavior: "smooth"
                    });
                });
            });

            smoothMobileMenuPoints.forEach( (point) => {
                point.addEventListener('click', (e) => {
                    e.preventDefault();
                    headerHeight = header.offsetHeight;
                    pointHref = point.getAttribute('href');
                    pointTopHeight = document.querySelector(pointHref).getBoundingClientRect().top + window.scrollY - headerHeight - 30;
                    siteUrl = document.URL.split('#')[0];
                    newUrl = siteUrl + pointHref;

                    window.history.pushState({}, '', newUrl);
                    
                    window.scrollTo({
                        top: pointTopHeight,
                        behavior: "smooth"
                    });
                });
            });
        }
    }

    scrollDownInit(){
        let scrollBtn = document.querySelector('.gti-scroll__down');
        if( !scrollBtn ) return false;

        scrollBtn.addEventListener('click', () => {
            let windowHeight = window.innerHeight;

            window.scrollTo({
                top: windowHeight,
                behavior: "smooth"
            });
        });
    }

    aboutSliderInit(){
        let sliders = document.querySelectorAll('.gti-about__company__slider'),
            aboutSlider;
        if( !sliders ) return false;
        sliders.forEach( (slider) => {
            aboutSlider = new Swiper(slider, {
                slidesPerView: 1.4,
                spaceBetween: 20,
                speed: 500,
                navigation: {
                    nextEl: slider.closest('.gti-about__company__slider__wrapper').querySelector('.swiper-button-next'),
                    prevEl: slider.closest('.gti-about__company__slider__wrapper').querySelector('.swiper-button-prev')
                },
                scrollbar: {
                    el: slider.closest('.gti-about__company__slider__wrapper').querySelector('.swiper-scrollbar')
                },
                breakpoints: {
                    991: {
                        spaceBetween: 50,
                        slidesPerView: 1.4
                    }
                }
            });
        });
    }

    benefitsSliderInit(){
        let slider = document.querySelector('.gti-benefits__slider');
        if( !slider ) return false;
        let textSlider = new Swiper(slider, {
            slidesPerView: 1,
            speed: 700,
            navigation: false,
            autoplay: {
                delay: 5000
            },
            pagination: {
              el: '.swiper-pagination',
            },
        });
    }

    productionSliderInit(){
        let slider = document.querySelector('.gti-production__text__slider');
        if( !slider ) return false;
        let textSlider = new Swiper(slider, {
                slidesPerView: 1,
                speed: 700,
                navigation: false,
                autoplay: {
                    delay: 5000
                }
            }),
            imageSlider = new Swiper(document.querySelector('.gti-production__slider'), {
                slidesPerView: 1,
                speed: 700,
                navigation: false,
                effect: 'fade',
                thumbs: {
                  swiper: textSlider,
                },
            }),
            pointsBlocks = document.querySelectorAll('.gti-production__text__points'),
            points, activeSlide, index;

        textSlider.on('slideChange', function (e) {
            activeSlide = e.activeIndex;
            pointsBlocks.forEach( (pointsBlock) => {
                points = pointsBlock.querySelectorAll('.point');
                points.forEach( (point) => {
                    point.classList.remove('active');
                });
            });

            pointsBlocks.forEach( (pointsBlock) => {
                points = pointsBlock.querySelectorAll('.point');
                points[activeSlide].classList.add('active');
            });
            
            imageSlider.slideTo(activeSlide);
        });

        pointsBlocks.forEach( (pointsBlock) => {
            points = pointsBlock.querySelectorAll('.point');

            points.forEach( (point) => {
                point.addEventListener('click', () => {
                    index = Array.from(point.closest('.gti-production__text__points').querySelectorAll('.point')).indexOf(point);

                    console.log(index);

                    textSlider.slideTo(index);
                    imageSlider.slideTo(index);
                });
            });
        });
    }
}

let general = new GeneralClass();