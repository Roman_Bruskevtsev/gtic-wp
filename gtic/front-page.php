<?php
/**
 * @package WordPress
 * @subpackage GTIC
 * @since 1.0
 * @version 1.0
 */

get_header(); 

if( have_rows('content') ):
    while ( have_rows('content') ) : the_row();
        if( get_row_layout() == 'hero_section' ): 
            get_template_part( 'template-parts/page/hero_section' );
        elseif( get_row_layout() == 'location_section' ): 
            get_template_part( 'template-parts/page/location_section' );
        elseif( get_row_layout() == 'about_company_section' ): 
            get_template_part( 'template-parts/page/about_company_section' );
        elseif( get_row_layout() == 'benefits_section' ): 
            get_template_part( 'template-parts/page/benefits_section' );
        elseif( get_row_layout() == 'production_section' ): 
            get_template_part( 'template-parts/page/production_section' );
        elseif( get_row_layout() == 'packaging_section' ): 
            get_template_part( 'template-parts/page/packaging_section' );
        elseif( get_row_layout() == 'b2b_section' ): 
            get_template_part( 'template-parts/page/b2b_section' );
        elseif( get_row_layout() == 'appointment_section' ): 
            get_template_part( 'template-parts/page/appointment_section' );
        endif;
    endwhile;
endif;

get_footer();