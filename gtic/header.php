<?php
/**
 * @package WordPress
 * @subpackage GTIC
 * @since 1.0
 * @version 1.0
 */
$header = get_field('header', 'option');
$logo = $header['logo'];
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <?php wp_body_open(); ?>
    <header class="gti-header" data-aos="fade-down" data-aos-delay="200" data-aos-duration="500">
        <div class="container">
            <div class="row">
                <?php if( $logo ) { ?>
                <div class="col-5 col-sm-6 col-md-6 col-lg-10">
                    <a class="gti-logo float-start" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <img src="<?php echo $logo['url']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
                    </a>
                <?php } 
                if( has_nav_menu('main') ) {
                    wp_nav_menu( array(
                        'theme_location'        => 'main',
                        'container'             => 'nav',
                        'container_class'       => 'gti-main__nav d-none d-lg-block float-start'
                    ) ); ?>
                </div>
                <?php } ?>
                <div class="col-7 col-sm-6 col-md-6 col-lg-2">
                    <div class="gti-right__nav float-end">
                        <?php if( $header['facebook'] ) { ?>
                            <a class="facebook" href="<?php echo $header['facebook']; ?>" target="_blank"></a>
                        <?php } 
                        $languages = pll_the_languages( array('raw' => 1) );
                        if( $languages ){
                            $current_lang = $lang_list = '';
                            foreach ( $languages as $lang ) {
                                if( !$lang['current_lang'] ){
                                    $lang_list .= '<li><a href="'.$lang['url'].'">'.$lang['name'].'</a></li>';
                                }
                            } ?>
                            <div class="language__switcher d-none d-lg-block">
                                <ul>
                                    <?php echo $lang_list; ?>
                                </ul>
                            </div>
                            <?php 
                            $current_lang = $lang_list = '';
                            foreach ( $languages as $lang ) {
                                if( !$lang['current_lang'] ){
                                    $lang_list .= '<li><a href="'.$lang['url'].'">'.$lang['slug'].'</a></li>';
                                }
                            } ?>
                            <div class="language__switcher d-block d-lg-none">
                                <ul>
                                    <?php echo $lang_list; ?>
                                </ul>
                            </div>
                        <?php } ?>
                        <div class="gti-mobile__btn d-block d-lg-none"><span></span><span></span><span></span></div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <?php
    if( has_nav_menu('main') ) {
        wp_nav_menu( array(
            'theme_location'        => 'main',
            'container'             => 'nav',
            'container_class'       => 'gti-mobile__nav d-block d-lg-none'
        ) ); 
    } ?>
    <main>