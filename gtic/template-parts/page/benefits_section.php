<?php 
$background = get_sub_field('background_image') ? ' style="background-image: url('.get_sub_field('background_image').')"' : '';
$anchor = get_sub_field('anchor') ? ' id="'.get_sub_field('anchor').'"' : '';
?>
<section class="gti-benefits__section background"<?php echo $background; ?>>
	<div class="container">
		<?php if( get_sub_field('small_title') || get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col-lg-12">
				<div class="anchor"<?php echo $anchor; ?>></div>
				<div class="gti-section__title noseparator text-center" data-aos="fade-up" data-aos-delay="500" data-aos-duration="500">
					<?php if( get_sub_field('small_title') ) { ?>
						<h4 class="font__red"><?php the_sub_field('small_title'); ?></h4>
					<?php }
					if( get_sub_field('title') ) { ?>
						<h2><?php the_sub_field('title'); ?></h2>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php } ?>
		<div class="row">
		<?php if( get_sub_field('image') ) { ?>
			<div class="col-lg-6 order-lg-2 order-1">
				<div class="image">
					<img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['title']; ?>">
				</div>
			</div>
		<?php } 
		$i = 1;
		if( get_sub_field('benefits') ) { ?>
			<?php foreach ( get_sub_field('benefits') as $benefit ) { 
				if( $i == 1 ) { ?>
					<div class="col-lg-3 order-lg-1 order-2  d-none d-lg-block">
				<?php } ?>

				<?php if( $i == 4 ) { ?>
					</div>
					<div class="col-lg-3 order-3  d-none d-lg-block">
				<?php } ?>
					<div class="gti-benefit__block" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500">
						<div class="title">
							<span><?php echo $i; ?></span>
							<?php if( $benefit['title'] ) { ?>
								<h4><?php echo $benefit['title']; ?></h4>
							<?php } ?>
						</div>
						<?php if( $benefit['text'] ) { ?>
						<div class="text"><?php echo $benefit['text']; ?></div>
						<?php } ?>
					</div>
				<?php if( $i == 6 ) { ?>
					</div>
				<?php } ?>
				
			<?php $i++; } 
		} ?>
		</div>
		<?php if( get_sub_field('benefits') ) { ?>
		<div class="row d-block d-lg-none">
			<div class="col">
				<div class="gti-benefits__slider swiper-container">
					<div class="swiper-wrapper">
					<?php 
					$i = 1;
					foreach ( get_sub_field('benefits') as $benefit ) { ?>
						<div class="swiper-slide">
							<div class="gti-benefit__slide text-center">
								<div class="title">
									<span><?php echo $i; ?></span>
									<?php if( $benefit['title'] ) { ?>
										<h4><?php echo $benefit['title']; ?></h4>
									<?php } ?>
								</div>
								<?php if( $benefit['text'] ) { ?>
								<div class="text"><?php echo $benefit['text']; ?></div>
								<?php } ?>
							</div>
						</div>
					<?php $i++; } ?>
					</div>
					<div class="swiper-pagination"></div>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>