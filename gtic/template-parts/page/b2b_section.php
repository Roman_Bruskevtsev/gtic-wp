<?php 
$background = get_sub_field('background_image') ? ' style="background-image: url('.get_sub_field('background_image').')"' : '';
$anchor = get_sub_field('anchor') ? ' id="'.get_sub_field('anchor').'"' : '';
?>
<section class="gti-b2b__section background"<?php echo $background; ?>>
	<div class="container">
		<?php if( get_sub_field('small_title') || get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col-md-6 col-lg-4">
				<div class="content">
					<div class="anchor"<?php echo $anchor; ?>></div>
					<div class="gti-section__title" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500">
						<?php if( get_sub_field('small_title') ) { ?>
							<h4 class="font__red"><?php the_sub_field('small_title'); ?></h4>
						<?php }
						if( get_sub_field('title') ) { ?>
							<h2><?php the_sub_field('title'); ?></h2>
						<?php } 
						if( get_sub_field('text') ) { ?>
							<div class="text"><?php the_sub_field('text'); ?></div>
						<?php } 
						if( get_sub_field('list') ) { ?>
							<div class="list">
								<ul>
									<?php foreach( get_sub_field('list') as $item ) { ?>
										<li><?php echo $item['item']; ?></li>
									<?php } ?>
								</ul>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>