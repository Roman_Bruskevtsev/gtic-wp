<?php 
$background = get_sub_field('background_image') ? ' style="background-image: url('.get_sub_field('background_image').')"' : '';
$anchor = get_sub_field('anchor') ? ' id="'.get_sub_field('anchor').'"' : '';
?>
<section class="gti-packaging__section"<?php echo $background; ?>>
	<div class="container">
		<?php if( get_sub_field('small_title') || get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col-md-12">
				<div class="anchor"<?php echo $anchor; ?>></div>
				<div class="gti-section__title text-center noseparator" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500">
					<?php if( get_sub_field('small_title') ) { ?>
						<h4 class="font__red"><?php the_sub_field('small_title'); ?></h4>
					<?php }
					if( get_sub_field('title') ) { ?>
						<h2><?php the_sub_field('title'); ?></h2>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php } 
		if( get_sub_field('image') ) { ?>
		<div class="row">
			<div class="col-md-12">
				<div class="image">
					<img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['title']; ?>">
				</div>
			</div>
		</div>
		<?php } ?>
		<div class="row">
			<?php if( get_sub_field('text_1') ) { ?>
			<div class="col-md-6">
				<div class="text text__1" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500"><?php the_sub_field('text_1'); ?></div>
			</div>
			<?php } 
			if( get_sub_field('small_image') ) { ?>
			<div class="col-md-2 order-md-2">
				<div class="small__image">
					<img src="<?php echo get_sub_field('small_image')['url']; ?>" alt="<?php echo get_sub_field('small_image')['title']; ?>">
				</div>
			</div>
			<?php } 
			if( get_sub_field('text_2') ) { ?>
			<div class="col-md-4 order-lg-1">
				<div class="text text__2" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500"><?php the_sub_field('text_2'); ?></div>
			</div>
			<?php } ?>
		</div>
	</div>
</section>