<?php 
$background = get_sub_field('background_image') ? ' style="background-image: url('.get_sub_field('background_image').')"' : '';
$anchor = get_sub_field('anchor') ? ' id="'.get_sub_field('anchor').'"' : '';
$slider = get_sub_field('slider');
?>
<section class="gti-production__section"<?php echo $background; ?>>
	<div class="container">
		<div class="row">
			<div class="col-lg-4">
				<div class="anchor"<?php echo $anchor; ?>></div>
				<?php if( get_sub_field('small_title') || get_sub_field('title') ) { ?>
				<div class="gti-section__title noseparator" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500">
					<?php if( get_sub_field('small_title') ) { ?>
						<h4 class="font__red"><?php the_sub_field('small_title'); ?></h4>
					<?php }
					if( get_sub_field('title') ) { ?>
						<h2><?php the_sub_field('title'); ?></h2>
					<?php } ?>
				</div>
				<?php } 
				if( $slider ) { ?>
				<div class="gti-production__text__slider__wrapper">
					<div class="gti-production__text__points d-none d-lg-block">
						<div class="points__row">
							<?php 
							$i = 1;
							foreach ( get_sub_field('slider') as $point ) { ?>
							<div class="point<?php echo $i == 1 ? ' active' : ''; ?>">
								<div class="active" style="background-image: url('<?php echo $point['slide_number_active']; ?>');"></div>
								<div style="background-image: url('<?php echo $point['slide_number_not_active']; ?>');"></div>
							</div>
							<?php $i++; } ?>
						</div>
						<div class="line"></div>
					</div>
					<div class="gti-production__text__slider swiper-container">
						<div class="swiper-wrapper">
							<?php foreach ( get_sub_field('slider') as $slide ) { ?>
							<div class="swiper-slide">
								<?php if( $slide['image'] ) { ?>
								<div class="image d-block d-lg-none" style="background-image: url('<?php echo $slide['image']; ?>');"></div>
							<?php } ?>
								<div class="text">
									<?php if( $slide['title'] ) { ?>
										<h3><?php echo $slide['title']; ?></h3>
									<?php } 
									echo $slide['text']; ?>
								</div>
							</div>
							<?php } ?>
						</div>
					</div>
					<div class="gti-production__text__points d-block d-lg-none">
						<div class="points__row">
							<?php 
							$i = 1;
							foreach ( get_sub_field('slider') as $point ) { ?>
							<div class="point<?php echo $i == 1 ? ' active' : ''; ?>">
								<div class="active" style="background-image: url('<?php echo $point['slide_number_active']; ?>');"></div>
								<div style="background-image: url('<?php echo $point['slide_number_not_active']; ?>');"></div>
							</div>
							<?php $i++; } ?>
						</div>
						<div class="line"></div>
					</div>
				</div>
				<?php } ?>
			</div>
			<?php if( $slider ) { ?>
			<div class="col-lg-1"></div>
			<div class="col-lg-7 position-relative">
				<div class="gti-production__slider swiper-container d-none d-lg-block">
					<div class="swiper-wrapper">
						<?php foreach ( get_sub_field('slider') as $slide ) { ?>
						<div class="swiper-slide">
							<?php if( $slide['image'] ) { ?>
								<div class="image" style="background-image: url('<?php echo $slide['image']; ?>');"></div>
							<?php } ?>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</section>