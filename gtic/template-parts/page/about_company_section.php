<?php 
$anchor = get_sub_field('anchor') ? ' id="'.get_sub_field('anchor').'"' : '';
?>
<section class="gti-about__company__section">
	<?php if( get_sub_field('slider') ) { ?>
	<div class="container-fluid nopadding d-none d-lg-block">
		<div class="row">
			<div class="col-lg-4"></div>
			<div class="col-lg-8 position-relative">
				<div class="gti-about__company__slider__wrapper">
					<div class="gti-about__company__slider swiper-container">
						<div class="swiper-wrapper">
							<?php foreach ( get_sub_field('slider') as $slide ) { ?>
								<div class="swiper-slide">
									<div class="gti-about__company__image" style="background-image: url('<?php echo $slide; ?>');"></div>
								</div>
							<?php } ?>
						</div>
					</div>
					<div class="slider-navigation">
						<div class="swiper-scrollbar"></div>
						<div class="swiper-button-group">
							<div class="swiper-button-prev"></div>
	  						<div class="swiper-button-next"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php } 
	$background = get_sub_field('background') ? ' style="background-image: url('.get_sub_field('background').')"' : '';
	?>
	<div class="container">
		<div class="row">
			<div class="col-lg-4">
				<div class="content">
					<div class="layer"<?php echo $background; ?>></div>
					<div class="anchor"<?php echo $anchor; ?>></div>
					<?php if( get_sub_field('small_title') || get_sub_field('title') || get_sub_field('text') ) { ?>
					<div class="gti-section__title" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500">
						<?php if( get_sub_field('small_title') ) { ?>
							<h4 class="font__red"><?php the_sub_field('small_title'); ?></h4>
						<?php }
						if( get_sub_field('title') ) { ?>
							<h2><?php the_sub_field('title'); ?></h2>
						<?php }
						if( get_sub_field('text') ) { ?>
							<div class="text"><?php the_sub_field('text'); ?></div>
						<?php } ?>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
	<?php if( get_sub_field('slider') ) { ?>
	<div class="container-fluid nopadding d-block d-lg-none">
		<div class="row">
			<div class="col-lg-4"></div>
			<div class="col-lg-8 position-relative">
				<div class="gti-about__company__slider__wrapper">
					<div class="gti-about__company__slider swiper-container">
						<div class="swiper-wrapper">
							<?php foreach ( get_sub_field('slider') as $slide ) { ?>
								<div class="swiper-slide">
									<div class="gti-about__company__image" style="background-image: url('<?php echo $slide; ?>');"></div>
								</div>
							<?php } ?>
						</div>
					</div>
					<div class="slider-navigation">
						<div class="swiper-scrollbar"></div>
						<div class="swiper-button-group">
							<div class="swiper-button-prev"></div>
	  						<div class="swiper-button-next"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
</section>