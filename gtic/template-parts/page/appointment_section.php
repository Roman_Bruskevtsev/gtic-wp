<?php 
$background = get_sub_field('background_image') ? ' style="background-image: url('.get_sub_field('background_image').')"' : '';
$anchor = get_sub_field('anchor') ? ' id="'.get_sub_field('anchor').'"' : '';
?>
<section class="gti-appointment__section background"<?php echo $background; ?>>
	<div class="container">
		<div class="row">
			<div class="col-lg-4 order-lg-2">
				<div class="content">
					<div class="anchor"<?php echo $anchor; ?>></div>
					<div class="gti-section__title" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500">
						<?php if( get_sub_field('small_title') ) { ?>
							<h4 class="font__red"><?php the_sub_field('small_title'); ?></h4>
						<?php }
						if( get_sub_field('title') ) { ?>
							<h2><?php the_sub_field('title'); ?></h2>
						<?php } 
						the_sub_field('text'); ?>
					</div>
				</div>
			</div>
			<div class="col-lg-1"></div>
			<?php if( get_sub_field('image') ) { ?>
			<div class="col-lg-7 order-lg-1">
				<div class="image">
					<img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['title']; ?>">
				</div>
			</div>
			
			<?php } ?>
			
		</div>
	</div>
</section>