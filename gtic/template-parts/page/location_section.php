<?php 
$background = get_sub_field('background_image') ? ' style="background-image: url('.get_sub_field('background_image').')"' : '';
$anchor = get_sub_field('anchor') ? ' id="'.get_sub_field('anchor').'"' : '';
?>
<section class="gti-location__section"<?php echo $background; ?>>
	<div class="container">
		<div class="row">
			<div class="col-lg-4 order-lg-2">
				<div class="anchor"<?php echo $anchor; ?>></div>
				<?php if( get_sub_field('small_title') || get_sub_field('title') || get_sub_field('text') ) { ?>
				<div class="gti-section__title" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500">
					<?php if( get_sub_field('small_title') ) { ?>
						<h4 class="font__red"><?php the_sub_field('small_title'); ?></h4>
					<?php }
					if( get_sub_field('title') ) { ?>
						<h2><?php the_sub_field('title'); ?></h2>
					<?php }
					if( get_sub_field('text') ) { ?>
						<div class="text"><?php the_sub_field('text'); ?></div>
					<?php } ?>
				</div>
				<?php } ?>
			</div>
			<div class="col-lg-8 order-lg-1">
				<?php if( get_sub_field('image') ) { ?>
				<div class="gti-map__block">
					<img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['title']; ?>">
				</div>
				<?php } ?>
			</div>
		</div>
		<?php 
		if( get_sub_field('legends') ) { ?>
		<div class="row">
			<?php 
			$i = 100;
			foreach ( get_sub_field('legends') as $legend ) { ?>
				<div class="col-6 col-md-4 col-lg-2">
					<div class="gti-legend__block" data-aos="fade-up" data-aos-delay="<?php echo $i; ?>" data-aos-offset="-300" data-aos-duration="300">
						<?php if( $legend['icon'] ) { ?>
						<div class="icon">
							<img src="<?php echo $legend['icon']['url']; ?>" alt="<?php echo $legend['icon']['title']; ?>">
						</div>
						<?php } 
						if( $legend['name'] ) { ?>
							<h6><?php echo $legend['name']; ?></h6>
						<?php } ?>
					</div>
				</div>
			<?php $i = $i + 100; } ?>
		</div>
		<?php } ?>
	</div>
</section>