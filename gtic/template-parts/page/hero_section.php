<?php 
$background = get_sub_field('background_image') ? ' style="background-image: url('.get_sub_field('background_image').')"' : '';
$anchor = get_sub_field('anchor') ? ' id="'.get_sub_field('anchor').'"' : '';
?>
<section class="gti-hero__section background"<?php echo $background.$anchor; ?> id="hero">
	<div class="container">
		<div class="row">
			<div class="col-lg-2"></div>
			<div class="col-lg-8">
				<div class="content__wrapper">
					<div class="content"<?php echo $anchor; ?>>
						<?php if( get_sub_field('title') ) { ?>
							<h1 data-aos="fade-up" data-aos-delay="200" data-aos-offset="-300" data-aos-duration="500"><?php the_sub_field('title'); ?></h1>
						<?php } 
						if( get_sub_field('text') || get_sub_field('logo') ) { ?>
						<div class="text__row" data-aos="fade-up" data-aos-delay="200" data-aos-offset="-300" data-aos-duration="500">
							<div class="row">
								<div class="col-lg-9">
									<?php if( get_sub_field('text') ) { ?>
										<div class="text"><?php the_sub_field('text'); ?></div>
									<?php } ?>
								</div>
								<div class="col-lg-3">
									<?php if( get_sub_field('logo') ) { ?>
									<div class="logo">
										<img src="<?php echo get_sub_field('logo')['url']; ?>" alt="<?php echo get_sub_field('logo')['title']; ?>">
									</div>
									<?php } ?>
								</div>
							</div>
						</div>
						<?php } ?>
					</div>
					<div class="gti-scroll__down"><span class="icon"></span></div>
				</div>
			</div>
		</div>
	</div>
</section>